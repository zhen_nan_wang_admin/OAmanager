package com.uplooking.mapper;

import com.uplooking.pojo.Attendance;
import com.uplooking.pojo.AttendanceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AttendanceMapper {
    int countByExample(AttendanceExample example);

    int deleteByExample(AttendanceExample example);

    int deleteByPrimaryKey(Integer kid);

    int insert(Attendance record);

    int insertSelective(Attendance record);

    List<Attendance> selectByExample(AttendanceExample example);

    Attendance selectByPrimaryKey(Integer kid);

    int updateByExampleSelective(@Param("record") Attendance record, @Param("example") AttendanceExample example);

    int updateByExample(@Param("record") Attendance record, @Param("example") AttendanceExample example);

    int updateByPrimaryKeySelective(Attendance record);

    int updateByPrimaryKey(Attendance record);

	Attendance findByUIdAndDate(@Param("uid") Integer uid,@Param("date") String date);

	List<Attendance> findAll(int uid);

	List<Attendance> findByReason(@Param("reason")String reason,@Param("uid")int uid);

	List<Attendance> findByMonthAndReason(@Param("month") String check_month,@Param("reason") String reason,@Param("uid")int uid);

	List<Attendance> findByDateAndReason(@Param("date")String check_date,@Param("reason") String reason,@Param("uid") int uid);

	int countByMonthAndIdAndType(@Param("thisMonth") String thisMonth,@Param("uid") Integer uid,@Param("type") int type);

	int updateByIdAndDateAndReason(@Param("uid") int uid,@Param("date") String date,@Param("reason") String reason);

	int checkType(@Param("uid")int uid,@Param("date") String date);

	List<Attendance> findByDateAndName(@Param("check_date")String check_date,@Param("name") String name,@Param("gid") int gid);

	List<Attendance> findByGroup(@Param("gid")int gid);

}