package com.uplooking.jmsConsumer;

import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.uplooking.utils.MyEmail;

public class JmsEmailListener implements MessageListener {
	
	@Autowired
	private MyEmail MyEmail;
 
    public void onMessage(Message message) {
        TextMessage Msg = (TextMessage) message;
        //从ActiveMQ监听到发送信息的请求
        try {
            System.out.println("接收者受到消息：" + Msg.getText());
            
            Map<String, Object> map =JSON.parseObject(Msg.getText());
            MyEmail.sendMail((String)map.get("email"),(Integer) map.get("code"));
            System.out.println("发送成功");
            
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
 
}