package com.uplooking.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageInfo;
import com.uplooking.pojo.Customer;
import com.uplooking.pojo.User;
import com.uplooking.service.CustomerService;
import com.uplooking.utils.AjaxReturn;
import com.uplooking.utils.JsonUtils;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;


/**
    *    客户controller
 * @author xuwenhao
 *
 */
@Controller
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	
	/**
	   *      客户列表
	 * @param pageNum
	 * @param cname
	 * @param company
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("customer_list")
	public String customerList(@RequestParam(value="pageNum",defaultValue="1")int pageNum,String cname,
			String company,HttpServletRequest req,HttpServletResponse resp) {
		
		PageInfo pageInfo = customerService.findByPage(pageNum,cname);
		
		req.setAttribute("customerPageInfo", pageInfo);
		req.setAttribute("cname", cname);
		return "customer/customer-list";
	}
	
	/**
	 * 更新跟进客户
	 * @param req
	 * @param cid
	 * @return
	 */
	@RequestMapping("update_follow")
	@ResponseBody
	public Object updateFollow(HttpServletRequest req,int cid) {
		AjaxReturn ax = new AjaxReturn();
        User user = (User) req.getSession().getAttribute("user");
        //
		Customer c = new Customer();
		c.setCid(cid);
		c.setIsfollow(2);
		c.setManageId(user.getUid());
		int result = customerService.updateCustomer(c);
		if(result==1) {
			ax.setHead("ok");
			ax.setMsg("跟进成功");
		}else {
			ax.setHead("err");
			ax.setMsg("跟进失败");
		}
		return JsonUtils.objectToJson(ax);
	}
	
	/**
	 * 添加客户
	 * @param req
	 * @param cid
	 * @return
	 */
	@RequestMapping("add_customer")
	@ResponseBody
	public Object addCustomer(Customer customer,HttpServletRequest req) {
		AjaxReturn ax = new AjaxReturn();
        //
		customer.setIsfollow(1);
		int result = customerService.insertCustomer(customer);
		if(result==1) {
			ax.setHead("ok");
			ax.setMsg("新增成功");
		}else {
			ax.setHead("err");
			ax.setMsg("新增失败");
		}
		return JsonUtils.objectToJson(ax);
	}
	
	
	/**
	 * 添加客户
	 * @param req
	 * @param cid
	 * @return
	 */
	@RequestMapping("update_customer")
	@ResponseBody
	public Object updateCustomer(Customer customer,HttpServletRequest req) {
		AjaxReturn ax = new AjaxReturn();
        //
		int result = customerService.updateCustomer(customer);
		if(result==1) {
			ax.setHead("ok");
			ax.setMsg("修改成功");
		}else {
			ax.setHead("err");
			ax.setMsg("修改失败");
		}
		return JsonUtils.objectToJson(ax);
	}
	
	
	/**
	 * 删除客户
	 * @param req
	 * @param cid
	 * @return
	 */
	@RequestMapping("del_customer")
	@ResponseBody
	public Object delCustomer(int cid,HttpServletRequest req) {
		AjaxReturn ax = new AjaxReturn();
        //
		int result = customerService.delCustomer(cid);
		if(result==1) {
			ax.setHead("ok");
			ax.setMsg("删除成功");
		}else {
			ax.setHead("err");
			ax.setMsg("删除失败");
		}
		return JsonUtils.objectToJson(ax);
	}
	
	
	
	/**
	    * 批量上传
	 * @param req
	 * @param cid
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("excel_input")
	public String excelInput(MultipartFile myfile,HttpServletRequest req) throws Exception {
		
		if(myfile!=null) {
			InputStream in=myfile.getInputStream();
			ImportParams params = new ImportParams();
			
			List<Customer> list = ExcelImportUtil.importExcel(in, Customer.class, params);
			System.out.println("-------------------"+list.size());
			for (Customer customer : list) {
				System.out.println(customer.getCname()+"  "+customer.getCompany()+"  "+customer.getLevel());
			}
			if(list!=null && list.size()>0) {
	        	boolean result=customerService.batchInsert(list);
	        	if(result) {
	        		req.setAttribute("msg", "添加成功");
	        	}else {
	        		req.setAttribute("msg", "添加失败");
	        	}
	        	return "customer/customer-list";
	        }
		}
		
		req.setAttribute("msg", "读取文件失败");
		return "customer/customer-list";
	}

}
