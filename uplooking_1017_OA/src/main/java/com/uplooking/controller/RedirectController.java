package com.uplooking.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.pagehelper.PageInfo;
import com.uplooking.pojo.Customer;
import com.uplooking.pojo.MonthCount;
import com.uplooking.pojo.User;
import com.uplooking.service.AttendanceService;
import com.uplooking.service.CustomerService;
import com.uplooking.service.UserService;
import com.uplooking.utils.SimpleDateFormatUtil;

@Controller
@RequestMapping("redirect")
public class RedirectController {
	
	
	@Autowired
	private AttendanceService attendanceService;
	
	@Autowired
	private CustomerService customerService;
	
	
	@Autowired
	private UserService userService;
	
	/**
	 * 跳转到index.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toIndex")
	public String toIndex(HttpServletRequest req) {
		
		return "index";
	}
	
	/**
	 * 跳转到login.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toLogin")
	public String toLogin(HttpServletRequest req) {
		
		return "login";
	}
	
	/**
	 * 跳转到welcome.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toWelcome")
	public String toWelcome(HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("user");
		if(user!=null){
			return "welcome";
		}else{
			return "redirect:toCannotGo";
		}
		
	}
	
	/**
	 * 跳转到CannotGo.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toCannotGo")
	public String toCannotGo(HttpServletRequest req) {
			return "error/CannotGo";
		
	}
	
	
	/**
	 * 跳转到attendance-list.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toAttendanceList")
	public String toOrderlist(HttpServletRequest req,HttpServletResponse resp) {
		User user = (User) req.getSession().getAttribute("user");
		if(user!=null){
			String today = SimpleDateFormatUtil.smt2.format(new Date());
			String thisMonth = today.substring(0, 7);
			//月考勤对象
			MonthCount m = new MonthCount();
			m.setTotalCount(attendanceService.findByMonthAndIdAndType(thisMonth,user.getUid(),0)) ;
			m.setOnTimeCount(attendanceService.findByMonthAndIdAndType(thisMonth,user.getUid(),1));
			m.setOutTimeWithoutReason(attendanceService.findByMonthAndIdAndType(thisMonth,user.getUid(),2)); 
			m.setOutTimeWithReasonAndWithoutAgree(attendanceService.findByMonthAndIdAndType(thisMonth,user.getUid(),3));  
			m.setOutTimeWithReasonAndWithAgree(attendanceService.findByMonthAndIdAndType(thisMonth,user.getUid(),4)); 
			req.setAttribute("monthCount", m);
			return "attendance/attendance-list";
		}else{
			return "redirect:toCannotGo";
		}
	}
	
	
	/**
	 * 跳转到forgetPwd.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toForgetPwd")
	public String toForgetPwd(HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("user");
		if(user!=null){
			return "user/forgetPwd";
		}else{
			return "redirect:toCannotGo";
		}
		
	}
	
	
	/**
	 * 跳转到UpdatePWd.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toUpdatePWd")
	public String toUpdatePWd(String uid,HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("user");
		if(user!=null){
			req.setAttribute("uid", uid);
			return "user/updatePwd";
		}else{
			return "redirect:toCannotGo";
		}
		
	}
	
	
	/**
	 * 跳转到addAttendance.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toAddAttendance")
	public String toAddAttendance(String uid,HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("user");
		if(user!=null){
			return "attendance/addAttendance";
		}else{
			return "redirect:toCannotGo";
		}
	}
	
	/**
	 * 跳转到team_list.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toTeamList")
	public String toTeamList(HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("user");
		if(user!=null){
			return "team/team_list";
		}else{
			return "redirect:toCannotGo";
		}
	}
	
	/**
	 * 跳转到team_add.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toTeamAdd")
	public String toTeamAdd(HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("user");
		if(user!=null){
			return "team/team_add";
		}else{
			return "redirect:toCannotGo";
		}
	}
	
	
	/**
	 * 跳转到team_update.jsp
	 * @param req
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("to_update_user")
	public String toUpdateUser(int uid,HttpServletRequest req,HttpServletResponse resp) throws IOException {
		User user2 = userService.findById(uid);
		if(user2!=null) {
			req.setAttribute("UpUser", user2);
			return "team/team_update";
		}else {
			resp.getWriter().write("找不到该用户");
			resp.getWriter().close();
			return null;
		}
	}
	
	
	/**
	 * 跳转到bossTeam_list.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toBossTeamList")
	public String toBossTeamList(HttpServletRequest req) {
		return "team/bossTeam_list";
	}
	
	
	
	/**
	 * 跳转到review.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toReview")
	public String toReview() {
		return "review";
	}
	
	/**
	 * 跳转到agree_review.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("toAgreeReview")
	public String toAgreeReview(int kid,HttpServletRequest req) {
		req.setAttribute("kid", kid);
		return "agree_review";
	}
	
	
	/**
	 * 跳转到customer-list.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("to_customer_list")
	public String toCustomerList(HttpServletRequest req) {
		return "customer/customer-list";
	}
	
	/**
	 * 跳转到customer-add.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("to_customer_add")
	public String toCustomerAdd(HttpServletRequest req) {
		return "customer/customer_add";
	}
	
	
	/**
	 * 跳转到customer-add.jsp
	 * @param req
	 * @return
	 */
	@RequestMapping("to_customer_update")
	public String toCustomerUpdate(HttpServletRequest req,int cid) {
		Customer customer =  customerService.findById(cid);
		if(customer!=null) {
			req.setAttribute("customer", customer);
			return "customer/customer_update";
		}
		req.setAttribute("msg","找不到该用户，请刷新");
		return "msg";
	}
	
	
}
