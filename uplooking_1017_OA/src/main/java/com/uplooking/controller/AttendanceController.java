package com.uplooking.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.uplooking.pojo.Attendance;
import com.uplooking.pojo.MonthCount;
import com.uplooking.pojo.User;
import com.uplooking.service.AttendanceService;
import com.uplooking.utils.AjaxReturn;
import com.uplooking.utils.JsonUtils;
import com.uplooking.utils.SimpleDateFormatUtil;

@Controller
public class AttendanceController {
	
	@Autowired
	private AttendanceService attendanceService;
	
	/**
	    *    个人考勤明细
	 * @param check_date
	 * @param check_month
	 * @param reason
	 * @param req
	 * @param resp
	 * @param pageNum
	 * @return
	 */
	@RequestMapping("findAttendance")
	public String findAttendance(String check_date,String check_month,String reason,
			HttpServletRequest req,HttpServletResponse resp,@RequestParam(value="pageNum",defaultValue="1") int pageNum) {
		User user = (User) req.getSession().getAttribute("user");
		//月考勤对象
		MonthCount m = new MonthCount();
		
		if("".equals(check_month)) {
			String today = SimpleDateFormatUtil.smt2.format(new Date());
			String thisMonth = today.substring(0, 7);
			m.setTotalCount(attendanceService.findByMonthAndIdAndType(thisMonth,user.getUid(),0)) ;
			m.setOnTimeCount(attendanceService.findByMonthAndIdAndType(thisMonth,user.getUid(),1));
			m.setOutTimeWithoutReason(attendanceService.findByMonthAndIdAndType(thisMonth,user.getUid(),2)); 
			m.setOutTimeWithReasonAndWithoutAgree(attendanceService.findByMonthAndIdAndType(thisMonth,user.getUid(),3));  
			m.setOutTimeWithReasonAndWithAgree(attendanceService.findByMonthAndIdAndType(thisMonth,user.getUid(),4));
		}else {
			m.setTotalCount(attendanceService.findByMonthAndIdAndType(check_month,user.getUid(),0)) ;
			m.setOnTimeCount(attendanceService.findByMonthAndIdAndType(check_month,user.getUid(),1));
			m.setOutTimeWithoutReason(attendanceService.findByMonthAndIdAndType(check_month,user.getUid(),2)); 
			m.setOutTimeWithReasonAndWithoutAgree(attendanceService.findByMonthAndIdAndType(check_month,user.getUid(),3));  
			m.setOutTimeWithReasonAndWithAgree(attendanceService.findByMonthAndIdAndType(check_month,user.getUid(),4));
		}

		PageInfo pageInfo= attendanceService.findByPage(pageNum,check_date,check_month,reason,user.getUid());
		req.setAttribute("monthCount", m);
		req.setAttribute("date", check_date);
		req.setAttribute("month", check_month);
		req.setAttribute("reason", reason);
		req.setAttribute("pageInfo", pageInfo);
		
		return "attendance/attendance-list";
	}
	
	/**
	   *    提交补考勤
	 * @param date
	 * @param reason
	 * @param req
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping("reviewAttendance")
	@ResponseBody
	public Object reviewAttendance(String date,String reason,HttpServletRequest req) throws ParseException {
		User user = (User) req.getSession().getAttribute("user");
		AjaxReturn ax= new AjaxReturn();
		
		Date adate = SimpleDateFormatUtil.smt2.parse(date);
		String today = SimpleDateFormatUtil.smt2.format(new Date());
		Date now = SimpleDateFormatUtil.smt2.parse(today);
		if(adate.getTime()>now.getTime()) {
			ax.setHead("err");
			ax.setMsg("不能补今天之后的缺勤");
			return JsonUtils.objectToJson(ax);
		}
		
		//检查考勤状态
		int type = attendanceService.checkType(user.getUid(),date);
		if(type==1) {
			ax.setHead("err");
			ax.setMsg("正常打卡，无须补考勤");
		}else if(type==2) {
			int result = attendanceService.submitAudit(user.getUid(),date,reason);
			if(result==1) {
				ax.setHead("ok");
				ax.setMsg("提交成功");
			}else {
				ax.setHead("err");
				ax.setMsg("提交失败");
			}
		}else if(type==3) {
			ax.setHead("err");
			ax.setMsg("不能重复补考勤");
		}else if(type==4) {
			ax.setHead("err");
			ax.setMsg("已审核，无须补考勤");
		}
		return JsonUtils.objectToJson(ax);
	}
	
	/**
	 * 队员该月考勤记录
	 * @param uid
	 * @param req
	 * @return
	 */
	@RequestMapping("check_user_attendance")
	public String checkUserAttendance(int uid,HttpServletRequest req) {
		//System.out.println(uid);
		//月考勤对象
		MonthCount m = new MonthCount();
		//获取当前月份
		String today = SimpleDateFormatUtil.smt2.format(new Date());
		String thisMonth = today.substring(0, 7);
		m.setTotalCount(attendanceService.findByMonthAndIdAndType(thisMonth,uid,0)) ;
		m.setOnTimeCount(attendanceService.findByMonthAndIdAndType(thisMonth,uid,1));
		m.setOutTimeWithoutReason(attendanceService.findByMonthAndIdAndType(thisMonth,uid,2)); 
		m.setOutTimeWithReasonAndWithoutAgree(attendanceService.findByMonthAndIdAndType(thisMonth,uid,3));  
		m.setOutTimeWithReasonAndWithAgree(attendanceService.findByMonthAndIdAndType(thisMonth,uid,4));
		
		req.setAttribute("monthCount", m);
		return "user/user_attendance";
	}
	
	/**
	 * 队员该月考勤记录
	 * @param uid
	 * @param req
	 * @return
	 */
	@RequestMapping("find_review")
	public String findReview(String check_date,String check_month,String name,
			HttpServletRequest req,HttpServletResponse resp,@RequestParam(value="pageNum",defaultValue="1") int pageNum) {
		User user = (User) req.getSession().getAttribute("user");
		PageInfo pageInfo=null;
		//判断用户职位
		if(user.getAid()==3) {
			System.out.println(user.getGid());
			 pageInfo= attendanceService.findReview(pageNum,check_date,check_month,name,user.getGid());
		}else {
			 pageInfo= attendanceService.findReview(pageNum,check_date,check_month,name,0);
		}
		
		req.setAttribute("date", check_date);
		req.setAttribute("month", check_month);
		req.setAttribute("name", name);
		req.setAttribute("reviewPageInfo", pageInfo);
		return "review";
	}
	
	
	/**
	 *  审批补考勤
	 * @param uid
	 * @param req
	 * @return
	 */
	@RequestMapping("agree_attendance")
	@ResponseBody
	public Object agreeAttendance(int agreetype,int kid,String opinion,HttpServletRequest req,HttpServletResponse resp) {
		AjaxReturn ax= new AjaxReturn();
		User user = (User) req.getSession().getAttribute("user");
		
		Attendance attendance = new Attendance();
		attendance.setKid(kid);
		attendance.setApprovalOpinion(opinion);
		attendance.setOpinionTime(new Date());
		if(agreetype==1) {
			attendance.setAttendanceType(4);
		}else {
			attendance.setAttendanceType(2);
		}
		attendance.setBossid(user.getUid());
		
		int result = attendanceService.updateAttendanceType(attendance);
		if(result==1) {
			ax.setHead("ok");
			ax.setMsg("审批成功");
		}else {
			ax.setHead("err");
			ax.setMsg("审批失败");
		}
		return JsonUtils.objectToJson(ax);
	}

}
