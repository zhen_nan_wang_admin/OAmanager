package com.uplooking.controller;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uplooking.pojo.JsonData;

@Controller
public class RedisController {
	
	@Resource(name="redisTemplate")
	private RedisTemplate redisTemplate;

	@RequestMapping("redisSet")
	@ResponseBody
	public Object test01() {
		
		if(redisTemplate==null){ 
			return JsonData.buildError("redisT没有实例化");
		}else{ 
			redisTemplate.opsForValue().set("xuwenhao", "徐文濠");
			return JsonData.buildSuccess();
		}
	}
	
	@RequestMapping("redisGet")
	@ResponseBody
	public Object test02() {
		
		if(redisTemplate==null){ 
			return JsonData.buildError("redisT没有实例化");
		}else{ 
			String str = (String) redisTemplate.opsForValue().get("xuwenhao");
			System.out.println(str);
			return JsonData.buildSuccess();
		}
	}
}
