package com.uplooking.service;

import com.github.pagehelper.PageInfo;
import com.uplooking.pojo.Attendance;

public interface AttendanceService {

	String insertAttendance(Attendance attendance2);

	PageInfo findByPage(int pageNum, String check_date, String check_month, String reason, int uid);

	int findByMonthAndIdAndType(String thisMonth, Integer uid, int type);

	int submitAudit(int uid, String date, String reason);

	int checkType(int uid, String date);

	PageInfo findReview(int pageNum, String check_date, String check_month, String name, int gid);

	int updateAttendanceType(Attendance attendance);


}
