package com.uplooking.service;

import javax.jms.Destination;

public interface JmsSendEmailService {
	void sendMessage(Destination destination, final String message);
}
