package com.uplooking.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uplooking.pojo.Customer;

public interface CustomerService {

	PageInfo findByPage(int pageNum, String cname);

	int updateCustomer(Customer c);

	int insertCustomer(Customer customer);

	Customer findById(int cid);

	int delCustomer(int cid);

	boolean batchInsert(List<Customer> list);

}
