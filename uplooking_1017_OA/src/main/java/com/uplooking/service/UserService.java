package com.uplooking.service;


import com.github.pagehelper.PageInfo;
import com.uplooking.pojo.Attendance;
import com.uplooking.pojo.User;

public interface UserService {

	User login(User user);

	Attendance isFirst(User user2, String date);

	User isMatch(String uname, String email);

	int updatePwdById(int uid, String pwd);

	PageInfo findByGroup(int gid,int pageNum,String name);

	int insertUser(User user);

	User findById(int uid);

	int updateUser(User user);




}
