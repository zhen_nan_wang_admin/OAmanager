package com.uplooking.service.impl;


import java.util.Date;
import java.util.List;

import org.aspectj.internal.lang.annotation.ajcPrivileged;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uplooking.mapper.AttendanceMapper;
import com.uplooking.mapper.UserMapper;
import com.uplooking.pojo.Attendance;
import com.uplooking.pojo.User;
import com.uplooking.pojo.UserExample;
import com.uplooking.pojo.UserExample.Criteria;
import com.uplooking.service.UserService;


@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private AttendanceMapper attendanceMapper;
	

	/**
	 * 用户登陆接口
	 */
	public User login(User user) {
		User user2 = userMapper.findByName(user.getUname());
		if(user2!=null && user.getPwd().equals(user2.getPwd())){
			return user2;
		}
		return null;
	}

	/**
	 * 是否第一次登陆接口
	 */
	public Attendance isFirst(User user2, String date) {
		// TODO Auto-generated method stub
		return attendanceMapper.findByUIdAndDate(user2.getUid(),date);
	}

	/**
	 * 用户邮箱匹配接口
	 */
	public User isMatch(String uname, String email) {
		User user =  userMapper.findByName(uname);
		if(email.equals(user.getEmail())) {
			return user;
		}
		return null;
	}

	/**
	 * 修改密码接口
	 */
	public int updatePwdById(int uid, String pwd) {
		User user = new User();
		user.setUid(uid);
		user.setPwd(pwd);
		userMapper.updateByPrimaryKeySelective(user);
		return 1;
	}

	/**
	 * 根据部门查询所有 的接口
	 */
	public PageInfo findByGroup(int gid,int pageNum,String name) {
		PageHelper.startPage(pageNum, 6);
		System.out.println("pageNum="+pageNum+"    name="+name);
		List<User> users = userMapper.findByGroup(gid,name);
		System.out.println(users.size());
		PageInfo<User> pageInfo = new PageInfo<User>(users);
		return pageInfo;
	}

	/**
	 * 新增用户 接口
	 */
	public int insertUser(User user) {
		//判段员工职位
		if(user.getAid()==2) {
			user.setPosition("员工");
		}else if(user.getAid()==3) {
			user.setPosition("经理");
		}else {
			user.setPosition("老板");
		}
		//获取当前时间
		user.setCreateTime(new Date());
		
		return userMapper.insertSelective(user);
	}

	/**
	 * 根据id查询用户 接口(non-Javadoc)
	 */
	public User findById(int uid) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(uid);
	}

	/**
	 *  更新用户 接口
	 */
	public int updateUser(User user) {
		// TODO Auto-generated method stub
		return userMapper.updateByPrimaryKeySelective(user);
	}


	
}
