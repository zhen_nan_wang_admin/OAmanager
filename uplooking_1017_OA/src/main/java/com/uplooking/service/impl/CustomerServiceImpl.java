package com.uplooking.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uplooking.mapper.CustomerMapper;
import com.uplooking.pojo.Customer;
import com.uplooking.service.CustomerService;


@Service
public class CustomerServiceImpl implements CustomerService {
	
	
	@Autowired
	private CustomerMapper customerMapper;

	/**
	 * 客户列表接口
	 */
	public PageInfo findByPage(int pageNum, String cname) {
		PageHelper.startPage(pageNum, 6);
		List<Customer> customers = customerMapper.findByCnameAndCompany(cname);
		PageInfo<Customer> pageInfo = new PageInfo<Customer>(customers);
		return pageInfo;
	}

	/**
	 * 跟进客户接口
	 */
	public int updateCustomer(Customer c) {

		return customerMapper.updateByPrimaryKeySelective(c);
	}
	
	
	/**
	 * 新增客户接口
	 */
	public int insertCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return customerMapper.insertSelective(customer);
	}

	/**
	 * 根据id找客户
	 */
	public Customer findById(int cid) {
		// TODO Auto-generated method stub
		return customerMapper.selectByPrimaryKey(cid);
	}

	/**
	 * 根据id删客户
	 */
	public int delCustomer(int cid) {
		// TODO Auto-generated method stub
		return customerMapper.deleteByPrimaryKey(cid);
	}

	/**\
	 * 批量导入
	 */
	public boolean batchInsert(List<Customer> list) {
		// TODO Auto-generated method stub
		return customerMapper.batchInsert(list)>0?true:false;
	}

}
