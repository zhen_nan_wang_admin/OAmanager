package com.uplooking.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uplooking.mapper.AttendanceMapper;
import com.uplooking.pojo.Attendance;
import com.uplooking.service.AttendanceService;

@Service
public class AttendanceServiceImpl implements AttendanceService {
	
	@Autowired
	private AttendanceMapper attendanceMapper;

	/**
	 * 用户打卡
	 */
	public String insertAttendance(Attendance attendance2) {
		// TODO Auto-generated method stub
		return attendanceMapper.insertSelective(attendance2)>0?"插入成功":"插入失败";
	}


	/**
	    * 按条件查询打卡记录
	 */
	public PageInfo findByPage(int pageNum, String check_date, String check_month, String reason,int uid) {

		List<Attendance> attendances=new ArrayList<Attendance>();
		if(!"".equals(check_date)&&"".equals(check_month)) {
			PageHelper.startPage(pageNum, 5);
			attendances=attendanceMapper.findByDateAndReason(check_date,reason,uid);
			
		}else if("".equals(check_date)&&!"".equals(check_month)) {
			PageHelper.startPage(pageNum, 5);
			attendances=attendanceMapper.findByMonthAndReason(check_month,reason,uid);
			
		}else if("".equals(check_date)&&"".equals(check_month)&&!"".equals(reason)) {
			PageHelper.startPage(pageNum, 5);
			attendances=attendanceMapper.findByReason(reason,uid);
			
		}else {
			PageHelper.startPage(pageNum, 5);
			attendances=attendanceMapper.findAll(uid);
		}
		
		PageInfo<Attendance> pageinfo = new PageInfo<Attendance>(attendances);
		return pageinfo;
	}


	/**
	 * 根据月份，id，类型查询数量
	 */
	public int findByMonthAndIdAndType(String thisMonth, Integer uid, int type) {
		
		return attendanceMapper.countByMonthAndIdAndType(thisMonth,uid,type);
	}


	/**
	   *    补考勤
	 */
	public int submitAudit(int uid, String date, String reason) {
		
		int result = attendanceMapper.updateByIdAndDateAndReason(uid,date,reason);
		System.out.println(result);
		return result;
	}


	/**
	 * 检查考勤状态   接口
	 */
	public int checkType(int uid, String date) {
		// TODO Auto-generated method stub
		return attendanceMapper.checkType(uid,date);
	}


	/**
	 * 审批列表   接口
	 */
	public PageInfo findReview(int pageNum, String check_date, String check_month, String name, int gid) {
		List<Attendance> attendances=new ArrayList<Attendance>();
		if(!"".equals(check_date)&&"".equals(check_month)) {
			PageHelper.startPage(pageNum, 5);
			attendances=attendanceMapper.findByDateAndName(check_date,name,gid);
			
		}else if("".equals(check_date)&&!"".equals(check_month)) {
			PageHelper.startPage(pageNum, 5);
			attendances=attendanceMapper.findByDateAndName(check_month,name,gid);
			
		}else if("".equals(check_date)&&"".equals(check_month)&&!"".equals(name)) {
			PageHelper.startPage(pageNum, 5);
			attendances=attendanceMapper.findByDateAndName(null,name,gid);
			
		}else {
			PageHelper.startPage(pageNum, 5);
			System.out.println("========================================================");
			attendances=attendanceMapper.findByGroup(gid);
		}
		
		PageInfo<Attendance> pageinfo = new PageInfo<Attendance>(attendances);
		return pageinfo;
	}


	/**、
	 * 审批补考勤   接口
	 */
	public int updateAttendanceType(Attendance attendance) {
		// TODO Auto-generated method stub
		return attendanceMapper.updateByPrimaryKeySelective(attendance);
	}





}
