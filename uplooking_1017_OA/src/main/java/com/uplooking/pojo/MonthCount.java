package com.uplooking.pojo;

public class MonthCount {
	
	private int totalCount;
	private int onTimeCount;
	private int outTimeWithoutReason;
	private int outTimeWithReasonAndWithoutAgree;
	private int outTimeWithReasonAndWithAgree;
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getOnTimeCount() {
		return onTimeCount;
	}
	public void setOnTimeCount(int onTimeCount) {
		this.onTimeCount = onTimeCount;
	}
	public int getOutTimeWithoutReason() {
		return outTimeWithoutReason;
	}
	public void setOutTimeWithoutReason(int outTimeWithoutReason) {
		this.outTimeWithoutReason = outTimeWithoutReason;
	}
	public int getOutTimeWithReasonAndWithoutAgree() {
		return outTimeWithReasonAndWithoutAgree;
	}
	public void setOutTimeWithReasonAndWithoutAgree(int outTimeWithReasonAndWithoutAgree) {
		this.outTimeWithReasonAndWithoutAgree = outTimeWithReasonAndWithoutAgree;
	}
	public int getOutTimeWithReasonAndWithAgree() {
		return outTimeWithReasonAndWithAgree;
	}
	public void setOutTimeWithReasonAndWithAgree(int outTimeWithReasonAndWithAgree) {
		this.outTimeWithReasonAndWithAgree = outTimeWithReasonAndWithAgree;
	}
	
	

}
