package com.uplooking.utils;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;


@Component("MyEmail")
public class MyEmail {
	
	@Autowired
	private JavaMailSender mailSender;
	
	private String email;
	
	private int code;
	
	public int sendMail(String email,int code) {
		
		this.email=email;
		
		this.code=code;
		
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
			
			public void prepare(MimeMessage mimeMessage) throws Exception {
				//设置发送人
				mimeMessage.setFrom(new InternetAddress("1271653306@qq.com"));
				//收件人  To:发送  BCC：密送 CC:抄送
				System.out.println(MyEmail.this.email);
				mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(MyEmail.this.email));
			    
			    //设置标题
				mimeMessage.setSubject("OA系统邮箱认证");
				
				//文本内容
				mimeMessage.setText("验证码:"+MyEmail.this.code);
				
			}
		};
		
		System.out.println(messagePreparator);
		
		try {
			//发送邮件
			mailSender.send(messagePreparator);
			
		}catch (Exception e) {
			return 0;
		}
		return 1;
	}

	public int getCode() {
		return code;
	}

	public void setCode() {
		this.code = (int)(Math.random()*9999)+100;
	}
	
	
	
	
	
	

}
