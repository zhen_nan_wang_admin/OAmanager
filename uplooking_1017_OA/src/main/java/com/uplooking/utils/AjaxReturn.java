package com.uplooking.utils;

public class AjaxReturn {
     private String head;
     private Object body;
     private String msg;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public Object getBody() {
		return body;
	}
	public void setBody(Object body) {
		this.body = body;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
     
     
}
