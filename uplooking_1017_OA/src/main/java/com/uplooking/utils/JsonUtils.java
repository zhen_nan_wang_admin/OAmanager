package com.uplooking.utils;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JsonUtils {

    // ����jackson����
    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * ������ת����json�ַ�����
     * <p>Title: pojoToJson</p>
     * <p>Description: </p>
     * @param data
     * @return
     */
    public static String objectToJson(Object data) {
    	try {
			String string = MAPPER.writeValueAsString(data);
			return string;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    /**
     * ��json�����ת��Ϊ����
     * 
     * @param jsonData json����
     * @param clazz �����е�object����
     * @return
     */
    public static <T> T jsonToPojo(String jsonData, Class<T> beanType) {
        try {
            T t = MAPPER.readValue(jsonData, beanType);
            return t;
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return null;
    }
    
    /**
     * ��json����ת����pojo����list
     * <p>Title: jsonToList</p>
     * <p>Description: </p>
     * @param jsonData
     * @param beanType
     * @return
     */
    public static <T>List<T> jsonToList(String jsonData, Class<T> beanType) {
    	JavaType javaType = MAPPER.getTypeFactory().constructParametricType(List.class, beanType);
    	try {
    		List<T> list = MAPPER.readValue(jsonData, javaType);
    		return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return null;
    }
    
    /**
     * json字符串转换为map
     *@descript
     *@param jsonStr
     *@return
     *@author lijianning
     *@date 2015年6月15日
     *@version 1.0v
     */
//   public static Map<String, Object> parseJSONMap(String jsonStr){  
//        Map<String, Object> map = new HashMap<String, Object>();  
//        try {
//			//最外层解析  
//			JSONObject json = JSONObject.fromObject(jsonStr);
//		for (Object k : json.keySet()) {
//		Object v = json.get(k);
//		//如果内层还是数组的话，继续解析  
//		if (v instanceof JSONArray) {
//		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
//		Iterator<JSONObject> it = ((JSONArray) v).iterator();
//		while (it.hasNext()) {
//		JSONObject JSON = it.next();
//		list.add(parseJSONMap(JSON.toString()));
//		}
//		map.put(k.toString(), list);
//		} else {
//		map.put(k.toString(), v);
//		}
//		} 
//		} catch (Exception e) {
//		map.put("exception", jsonStr);
//		}
//		return map;  
//   }  
    
}
