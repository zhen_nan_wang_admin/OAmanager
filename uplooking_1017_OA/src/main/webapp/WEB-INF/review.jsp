<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
  <head>
  	<base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/layer/layer.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
    layui.use('laydate', function(){
    	  var laydate = layui.laydate;
    	  laydate.render({
    	    elem: '#check_date' //指定元素
    	  });
    	  laydate.render({
      	    elem: '#check_month'
      	    ,type: 'month'
      	  });
    });
    </script>
  </head>
  
  <body>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a><cite>审核考勤</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    <div class="x-body">
    
	    <c:if test="${reviewPageInfo==null }">
	         <% request.getRequestDispatcher("/find_review").forward(request, response); %>
	    </c:if>
    
      
      <!-- 搜索表单 -->
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" id="form" action="${pageContext.request.contextPath }/find_review" onsubmit="return Submit()">
          <input class="layui-input" placeholder="按照日查" name="check_date" id="check_date" >
          <input class="layui-input" placeholder="按照月查" name="check_month" id="check_month" >
          <input type="text" id="name" name="name"  placeholder="按用户名" autocomplete="off" class="layui-input" style="width: 400px;">
          <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
        </form>
      </div>
      
      <!-- 补考勤 -->
        <!-- <button class="layui-btn layui-btn-danger" onclick="add()"><i class="layui-icon">&#xe654;</i>补考勤</button> -->
        <!-- <button class="layui-btn" onclick="x_admin_show('添加用户','./order-add.html')"><i class="layui-icon"></i>添加</button> -->
        <span class="x-right" style="line-height:40px">共有数据：${reviewPageInfo.total } 条</span>
      
      
      <!-- 列表 -->
      <table class="layui-table">
        <thead>
          <tr>
            <th>员工姓名</th>
            <th>所属部门</th>
            <th>打卡时间</th>
            <th>缺勤原因</th>
            <th>月考勤记录</th>
            <th>审批</th>
           <!--  <th >操作</th> -->
         </tr>
        </thead>
        <tbody>
        <c:if test="${reviewPageInfo!=null }">
        <c:forEach items="${reviewPageInfo.list }" var="r">
	          <tr>
	          <td>${r.user.rname }</td>
	          <td>${r.user.gid==1?'人事部':'开发部' }</td>
	          <td><fmt:formatDate value="${r.attendanceTime }" pattern="yyyy-MM-dd HH:mm:ss" />   </td>
	          <td>${r.exceptionExplain }</td>
	          <td style="text-align: center;"><input id="${r.empid }" class="layui-btn layui-btn-xs layui-icon findAttendance" onclick="" value="&#xe64c;查询" style="width: 70px;"></td>
	          <td style="text-align: center;"><input id="${r.kid }" class="layui-btn layui-btn-xs layui-icon review"  value="&#xe64c;审批" style="width: 70px;"></td>
	            <!-- <td class="td-manage">
	              <a title="查看"  onclick="x_admin_show('编辑','order-view.html')" href="javascript:;">
	                <i class="layui-icon">&#xe63c;</i>
	              </a>
	              <a title="删除" onclick="member_del(this,'要删除的id')" href="javascript:;">
	                <i class="layui-icon">&#xe640;</i>
	              </a>
	            </td> -->
	          </tr>
	     </c:forEach>
	     </c:if>
        </tbody>
      </table>
      <c:if test="${reviewPageInfo!=null }">
	      <div class="page">
	        <div>
	          <a class="prev" href="${pageContext.request.contextPath }/find_review?check_date=${date }&check_month=${month }&name=${name }&pageNum=${reviewPageInfo.prePage }">&lt;&lt;</a>
	          <c:forEach begin="1" end="${reviewPageInfo.pages }" step="1" var="i">
			  	<c:if test="${reviewPageInfo.pageNum==i }">
			  		<span class="current">${i}</span>
			  	</c:if>
			  	<c:if test="${reviewPageInfo.pageNum!=i }">
			  		<a class="num" href="${pageContext.request.contextPath }/find_review?check_date=${date }&check_month=${month }&name=${name }&pageNum=${i}">${i}</a>
			  	</c:if>
			  </c:forEach>
	          <a class="next" href="${pageContext.request.contextPath }/find_review?check_date=${date }&check_month=${month }&name=${name }&pageNum=${reviewPageInfo.nextPage }">&gt;&gt;</a>
	        </div>
	      </div>
      </c:if>
      
      

    </div>
    <script>
      layui.use('laydate', function(){
        var laydate = layui.laydate;
        
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });

       /*用户-停用*/
      function member_stop(obj,id){
          layer.confirm('确认要停用吗？',function(index){

              if($(obj).attr('title')=='启用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 5,time:1000});

              }else{
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 5,time:1000});
              }
              
          });
      }

	$(function(){
		$(".findAttendance").click(function(){
			//通过按钮获取用户id
			var userId = this.id;
			layer.open({
	    	      type: 2,
	    	      title: '查看个人考勤',
	    	      maxmin: true,
	    	      shadeClose: true, //点击遮罩关闭层
	    	      area : ['800px' , '520px'],
	    	      content: ['${pageContext.request.contextPath }/check_user_attendance?uid='+userId,'no']
	    	  });
	    });
		$(".review").click(function(){
			//通过按钮获取用户id
			var kId = this.id;
			layer.open({
	    	      type: 2,
	    	      title: '审批补考勤',
	    	      maxmin: true,
	    	      shadeClose: true, //点击遮罩关闭层
	    	      area : ['800px' , '320px'],
	    	      content: ['${pageContext.request.contextPath }/redirect/toAgreeReview?kid='+kId,'no']
	    	  });
	    });
	});


      
      //提交
      function Submit(){
    	var dateValue = $("#check_date").val();
    	var monthValue = $("#check_month").val();
    	if(dateValue.length!=0 && monthValue.length!=0){
    		layer.msg("不能同时指定日期和指定月份");
    		return false;
    	}
    	return true;
      }
    </script>

  </body>

</html>