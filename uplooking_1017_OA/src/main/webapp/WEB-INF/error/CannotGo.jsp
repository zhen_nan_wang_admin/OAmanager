﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<base href="<%=basePath%>">
<head>
<title>网页访问不鸟</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/error_all.css?t=201303212934">
</head>
<body class="error-404">
<div id="doc_main">
	
	<section class="bd clearfix">
		<div class="module-error">
			<div class="error-main clearfix">
				<div class="label"></div>
				<div class="info">
					<h1 class="title">啊哦，你访问的权限不够哦，请先登陆！</h1>
					<div class="reason">
						<p>可能的原因：</p>
						<p>1.你不是管理员。</p>
						<p>2.你不是用户。</p>
						<p>3.你是来乱搞的。</p>
						<p>4.你的Session过期了，请重新登陆。</p>
					</div>
					<div class="oper">
						<p><a href="${pageContext.request.contextPath }/redirect/toLogin" target="_top">返回登陆页面&gt;</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

</body></html>
