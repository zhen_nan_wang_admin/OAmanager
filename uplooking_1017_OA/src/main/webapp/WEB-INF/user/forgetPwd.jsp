<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta charset="utf-8">
	<title>OA管理后台</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/font.css">
	<link rel="stylesheet" href="css/xadmin.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/layer/layer.js"></script>
    <script src="lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>


</head>
<body>
	<!-- onsubmit="return SubmitCheck()" -->
	<form class="layui-form" action="checkUser" id="form" onsubmit="return SubmitCheck()">
		<div style="margin-left: 120px;margin-top: 100px;">
		  <div class="layui-form-item">
		    <label class="layui-form-label">用户名</label>
		    <div class="layui-input-block">
		      <input id="uname" type="text" name="uname" required  lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input" style="width: 300px;">
		    </div>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">邮箱</label>
		    <div class="layui-input-inline">
		      <input id="email" type="email" name="email" required lay-verify="required" placeholder="请输入邮箱" autocomplete="off" class="layui-input" style="width: 300px;">
		    </div>
		    <div class="layui-form-mid layui-word-aux" style="margin-left: 110px;margin-top: -10px;"><input type="button" id="sendMail" class="layui-btn layui-btn-primary" value="发送验证码" /></div>
		  </div>
		  <div class="layui-form-item" id="codeDiv" style="display: none;">
		    <label class="layui-form-label">验证码</label>
		    <div class="layui-input-inline">
		      <input id="code" type="text" name="code" required lay-verify="required" placeholder="请输入验证码" autocomplete="off" class="layui-input" style="width: 300px;">
		    </div>
		    <div class="layui-form-mid layui-word-aux" ></div>
		  </div>
		  <div class="layui-form-item" id="submitDiv"  style="display: none;">
		    <div class="layui-input-block">
		      <button class="layui-btn" lay-submit lay-filter="formDemo">下一步</button>
		      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		  </div>
		</div>
	</form>

</body>
<script type="text/javascript">
$(function(){
	$("#sendMail").removeAttr("disabled");
	
	$("#sendMail").click(function(){
		var codeValue = $("#email").val();
		var unameValue = $("#uname").val();
		if(unameValue.length==0){
			layer.msg('请先填入用户名');
		}else{
			if(codeValue.length!=0){
				var reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
				var r = codeValue.match(reg);
				if(r==null){
					layer.msg('邮箱格式不正确');
				}else{
					var loadindex=layer.msg('发送中，请稍等', {
						  icon: 16
						  ,time: 25*1000
						  ,shade: 0.01
					});
					
					$.ajax({
		        		url:"sendMail",
		        		type:"post",
		        		data:"email="+codeValue+"&uname="+unameValue,
		        		success:function(result){
		        			var obj=eval("("+result+")");
		        			if(obj.head=="ok"){
		        				layer.close(loadindex);
		        				layer.msg(obj.body);
		        				time(this);
		        				$("#codeDiv").removeAttr("style");
		        				$("#submitDiv").removeAttr("style");
		        			}else {
		        				layer.msg('发送失败!');
		        			}
		        		}
		        	});
				}
			}else{
				layer.msg('邮箱不能为空');
			}
		}
	});
		
});

layui.use(['form', 'layedit', 'laydate'], function(){  
	  var form = layui.form()  
	  ,layer = layui.layer  
	  ,layedit = layui.layedit  
	  ,laydate = layui.laydate;  
	   
	  //自定义验证规则  
	  form.verify({  
	        contact: function(value){  
	          if(value.length < 4){  
	            return '内容请输入至少4个字符';  
	          }  
	        }  
	        ,email: [/^[a-z0-9._%-]+@([a-z0-9-]+\.)+[a-z]{2,4}$|^1[3|4|5|7|8]\d{9}$/, '邮箱格式不对']  
	  });  
	    
	    
});  

function SubmitCheck(){
	$.ajax({
		url:"checkUser",
		type:"post",
		data:$("#form").serialize(),
		success:function(result){
			var obj=eval("("+result+")");
			if(obj.head=="ok"){
				layer.msg(obj.msg,{icon:16,time: 1*1000},function(){
					window.location.href="${pageContext.request.contextPath }/redirect/toUpdatePWd?uid="+obj.body;
				});
			}else {
				layer.msg(obj.msg);
			} 
		}
	});
	//ajax添加 ，不需要跳转页面 
	return false;
}


//验证码倒计时
var wait = 60;
function time(obj) {
	if(wait==0) {
		$("#sendMail").removeAttr("disabled");
		$("#sendMail").val("获取验证码");
		wait = 60;
	}else {
		$("#sendMail").attr("disabled","true");
		$("#sendMail").val(wait+"秒后重试");
		wait--;
		setTimeout(function() {		//倒计时方法
			time(obj);
		},1000);	//间隔为1s
	}
}

</script>


</html>