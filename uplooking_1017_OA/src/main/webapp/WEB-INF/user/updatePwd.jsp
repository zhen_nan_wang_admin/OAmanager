<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta charset="utf-8">
	<title>OA管理后台</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/font.css">
	<link rel="stylesheet" href="css/xadmin.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/layer/layer.js"></script>
    <script src="lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>


</head>
<body>

	<form class="layui-form" action="updatePwd" id="form" onsubmit="return SubmitUpdate()">
		<div style="margin-left: 120px;margin-top: 100px;">
			<input type="hidden" name="uid" value="${uid }">
		  <div class="layui-form-item">
		    <label class="layui-form-label">新密码</label>
		    <div class="layui-input-block">
		      <input id="pwd" type="password" name="pwd" required  lay-verify="required" placeholder="请输入新密码" autocomplete="off" class="layui-input" style="width: 300px;">
		    </div>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">确认密码</label>
		    <div class="layui-input-inline">
		      <input id="repwd" type="password" name="repwd" required lay-verify="required" placeholder="请确认新密码" autocomplete="off" class="layui-input" style="width: 300px;">
		    </div>
		  </div>
		  <div class="layui-form-item" id="submitDiv">
		    <div class="layui-input-block">
		      <button class="layui-btn" lay-submit lay-filter="formDemo">修改</button>
		      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		  </div>
		</div>
	</form>

</body>
<script type="text/javascript">
function SubmitUpdate(){
	$.ajax({
		url:"repairAttendance",
		type:"post",
		data:$("#form").serialize(),
		success:function(result){
			var obj=eval("("+result+")");
			if(obj.head=="ok"){
				layer.msg(obj.msg,{icon:16,time: 1*1000},function(){
					var index = layer.index;
					layer.msg(index);
					parent.layer.close(index);
				});
			}else {
				layer.msg(obj.msg);
			}
		}
	});
	//ajax添加 ，不需要跳转页面 
	return false;
}
</script>
</html>