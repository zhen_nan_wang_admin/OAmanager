<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
  <head>
  	<base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/layer/layer.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
    layui.use('laydate', function(){
    	  var laydate = layui.laydate;
    	  laydate.render({
    	    elem: '#check_date' //指定元素
    	  });
    	  laydate.render({
      	    elem: '#check_month'
      	    ,type: 'month'
      	  });
    });
    
    </script>
  </head>
  
  <body>
    <c:if test="${customerPageInfo==null }">
         <% request.getRequestDispatcher("/customer_list").forward(request, response); %>
    </c:if>
    <c:if test="${error!=null }">
    	<script type="text/javascript">
    	 var msg ="${msg}";
    	 layer.msg(msg);
    	</script>
    	<% request.removeAttribute("error"); %>
    </c:if>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a><cite>客户列表</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    <div class="x-body">
    
      <!-- 搜索表单 -->
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" id="form" action="${pageContext.request.contextPath }/customer_list">
          <input type="text" id="reason" name="cname"  placeholder="客户名称或公司名称" autocomplete="off" class="layui-input" style="width: 400px;">
          <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
        </form>
      </div>
      
      <!-- 导出导入 -->
      <div style="width: 100%;height: 100px;background-color: #EEEEEE" >
      	<div style="width: 30%;float: left;">
          	<form action="excel_input" method="post" enctype="multipart/form-data" onsubmit="return Check()">
		        <input id="excel" type="file" name="myfile" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
		        <button class="layui-btn layui-btn-danger" type="submit"><i class="layui-icon">&#xe664;</i>批量导入</button>
	    	</form>
	    </div>
	    <div style="float: left;">
		   	<button class="layui-btn layui-btn-danger" onclick="Out()"><i class="layui-icon">&#xe654;</i>批量导出</button>
	      	<button class="layui-btn layui-btn-danger" onclick="Download()"><i class="layui-icon">&#xe674;</i>下载模板</button>
	    </div>
        <span class="x-right" style="line-height:40px">共有数据：${customerPageInfo.total } 条</span>
      <div>
      
      <!-- 列表 -->
      
      <table class="layui-table">
        <thead>
          <tr>
            <th>客户编号</th>
            <th>客户名称</th>
            <th>所属单位</th>
            <th>客户等级</th>
            <th>是否跟进</th>
            <th>跟进领导</th>
            <th>跟进</th>
            <th>操作</th>
         </tr>
        </thead>
        <tbody>
        <c:if test="${customerPageInfo !=null }">
	        <c:forEach items="${customerPageInfo.list }" var="c">
	          <tr>
	            <td>${c.cid }</td>
	            <td>${c.cname }</td>
	            <td>${c.company }</td>
	            <td>${c.level }</td>
	            <td><c:if test="${c.isfollow==1 }">未跟进</c:if><c:if test="${c.isfollow==2 }">已跟进</c:if></td>
	            <td>${c.user.rname }</td>
	            <td style="text-align: center;"><c:if test="${c.isfollow==1 }"><input id="${c.cid }" class="layui-btn layui-btn-xs layui-icon follow" onclick="" value="&#xe64c;跟进" style="width: 70px;"></c:if></td>
	            <td class="td-manage">
	              <a title="编辑" id="${c.cid }" onclick="member_update(this)" href="javascript:;">
	                <i class="layui-icon">&#xe63c;</i>
	              </a>
	              <a title="删除" id="${c.cid }" onclick="member_del(this)" href="javascript:;">
	                <i class="layui-icon">&#xe640;</i>
	              </a>
	            </td>
	          </tr>
	          </c:forEach>
          </c:if>
        </tbody>
      </table>
      
      <c:if test="${customerPageInfo!=null }">
	      <div class="page">
	        <div>
	          <a class="prev" href="${pageContext.request.contextPath }/customer_list?cname=${cname }&company=${company }&pageNum=${customerPageInfo.prePage }">&lt;&lt;</a>
	          <c:forEach begin="1" end="${customerPageInfo.pages }" step="1" var="i">
			  	<c:if test="${customerPageInfo.pageNum==i }">
			  		<span class="current">${i}</span>
			  	</c:if>
			  	<c:if test="${customerPageInfo.pageNum!=i }">
			  		<a class="num" href="${pageContext.request.contextPath }/customer_list?cname=${cname }&company=${company }&pageNum=${i}">${i}</a>
			  	</c:if>
			  </c:forEach>
	          <a class="next" href="${pageContext.request.contextPath }/customer_list?cname=${cname }&company=${company }&pageNum=${customerPageInfo.nextPage }">&gt;&gt;</a>
	        </div>
	      </div>
      </c:if>
      
      

    </div>
    <script>
      layui.use('laydate', function(){
        var laydate = layui.laydate;
        
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });
      
      function Check(){
    	  var value = $("#excel").val();
    	  if(value==''){
    		  layer.msg("请先导入excel表格");
    		  return false;
    	  }
      }

       /*用户-停用*/
      function member_stop(obj,id){
          layer.confirm('确认要停用吗？',function(index){

              if($(obj).attr('title')=='启用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 5,time:1000});

              }else{
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 5,time:1000});
              }
              
          });
      }

      /*用户-删除*/
      function member_del(obj){
    	  var cId = obj.id;
          layer.confirm('确认要删除吗？',function(index){
        	  $.ajax({
			    	url:"del_customer",
			    	type:"post",
			    	data:"cid="+cId,
			    	success:function(result){
			    		var obj=eval("("+result+")");
			    		if(obj.head=="ok"){
			    			layer.msg(obj.msg,{icon:16,time: 1*1000},function(){
								var index = layer.index;
								location.reload();
								parent.layer.close(index);
							});
			    		}else{
			    			layer.msg(obj.msg);
			    		}
			    	}
			    });
          });
      }
      
      /*用户-编辑*/
      function member_update(obj){
    	  var cid = obj.id;
    	  layer.open({
    	      type: 2,
    	      title: '编辑客户',
    	      maxmin: true,
    	      shadeClose: true, //点击遮罩关闭层
    	      area : ['800px' , '420px'],
    	      content: ['${pageContext.request.contextPath }/redirect/to_customer_update?cid='+cid,'no']
    	  });
      }



      function add() {
    	  layer.open({
    	      type: 2,
    	      title: '补考勤',
    	      maxmin: true,
    	      shadeClose: true, //点击遮罩关闭层
    	      area : ['800px' , '420px'],
    	      content: ['${pageContext.request.contextPath }/redirect/toAddAttendance','no']
    	  });
      }
      
      //提交
      function Submit(){
    	var dateValue = $("#check_date").val();
    	var monthValue = $("#check_month").val();
    	if(dateValue.length!=0 && monthValue.length!=0){
    		layer.msg("不能同时指定日期和指定月份");
    		return false;
    	}
    	return true;
      }
      
      /* 更新跟进状态 */
      $(function(){
    	  $(".follow").click(function(){
				//通过按钮获取用户id
				var cId = this.id;
				layer.msg('是否跟进', {
					  time: 0 //不自动关闭
					  ,btn: ['是', '否']
					  ,yes: function(index){
					    layer.close(index);
					    $.ajax({
					    	url:"update_follow",
					    	type:"post",
					    	data:"cid="+cId,
					    	success:function(result){
					    		var obj=eval("("+result+")");
					    		if(obj.head=="ok"){
					    			layer.msg(obj.msg,{icon:16,time: 1*1000},function(){
										var index = layer.index;
										location.reload();
										parent.layer.close(index);
									});
					    		}else{
					    			layer.msg(obj.msg);
					    		}
					    	}
					    });
					  }
				});
    	  });
      });
      
      
      //下载模板
      function Download(){
    	  window.location.href="excel/customer.xlsx";
      }
      
      
    </script>

  </body>

</html>