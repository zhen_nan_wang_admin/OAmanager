<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
  <head>
  	<base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/layer/layer.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <div class="x-body">
        <form class="layui-form" id="form" action="add_customer" onsubmit="return AddCustomer()">
          <!-- 登陆名 
          <div class="layui-form-item">
              <label for="uname" class="layui-form-label">
                  <span class="x-red">*</span>登录名
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="uname" name="uname" required="" lay-verify="required"
                  autocomplete="off" class="layui-input">
              </div>
              <div class="layui-form-mid layui-word-aux">
                  <span class="x-red">*</span>将会成为您唯一的登入名
              </div>
          </div>-->
          <!-- 客户名称 -->
          <div class="layui-form-item">
              <label for="cname" class="layui-form-label">
                  <span class="x-red">*</span>客户名称
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="cname" name="cname" required="" lay-verify="required"
                  autocomplete="off" class="layui-input">
              </div>
          </div>
          <!-- 所属单位 -->
          <div class="layui-form-item">
              <label for="company" class="layui-form-label">
                  <span class="x-red">*</span>所属单位
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="company" name="company" required="" lay-verify="required"
                  autocomplete="off" class="layui-input">
              </div>
          </div>
          <!-- 等级 -->
          <div class="layui-form-item">
              <label class="layui-form-label"><span class="x-red">*</span>等级</label>
              <div class="layui-input-block">
                <input type="radio" name="level" value="1" lay-skin="primary" title="1" >
                <input type="radio" name="level" value="2" lay-skin="primary" title="2" >
                <input type="radio" name="level" value="3" lay-skin="primary" title="3" >
                <input type="radio" name="level" value="4" lay-skin="primary" title="4" >
                <input type="radio" name="level" value="5" lay-skin="primary" title="5" >
              </div>
          </div>
          
          <div class="layui-form-item">
              <label for="L_repass" class="layui-form-label">
              </label>
              <button  class="layui-btn" lay-filter="add" lay-submit="">增加</button>
          </div>
      </form>
      
      
      
      
    </div>
    <script>
        layui.use(['form','layer'], function(){
          $ = layui.jquery;
          var form = layui.form
          ,layer = layui.layer;
        
          //自定义验证规则
          form.verify({
            nikename: function(value){
              if(value.length < 5){
                return '昵称至少得5个字符啊';
              }
            }
            ,pass: [/(.+){6,12}$/, '密码必须6到12位']
            ,repass: function(value){
                if($('#L_pass').val()!=$('#L_repass').val()){
                    return '两次密码不一致';
                }
            }
            ,mobile: [/^1(3|4|5|7|8)\d{9}$/, '手机号码格式不正确']
          });

        });
        
     function AddCustomer(){
    	 $.ajax({
    			url:"add_customer",
    			type:"post",
    			data:$("#form").serialize(),
    			success:function(result){
    				var obj=eval("("+result+")");
    				if(obj.head=="ok"){
    					layer.msg(obj.msg,{icon:16,time: 1*1000},function(){
    						window.location.href="${pageContext.request.contextPath }/customer_list";
    					});
    				}else {
    					layer.msg(obj.msg);
    				} 
    			}
    		});
    		//ajax添加 ，不需要跳转页面 
    	return false; 
     }   
        
    </script>

  </body>

</html>