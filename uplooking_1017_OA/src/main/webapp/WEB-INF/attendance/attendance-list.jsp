<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
  <head>
  	<base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/layer/layer.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
    layui.use('laydate', function(){
    	  var laydate = layui.laydate;
    	  laydate.render({
    	    elem: '#check_date' //指定元素
    	  });
    	  laydate.render({
      	    elem: '#check_month'
      	    ,type: 'month'
      	  });
    });
    </script>
  </head>
  
  <body>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a><cite>个人考勤明细</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    <div class="x-body">
    
      <!-- 搜索表单 -->
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" id="form" action="${pageContext.request.contextPath }/findAttendance" onsubmit="return Submit()">
          <input class="layui-input" placeholder="按照日查" name="check_date" id="check_date" >
          <input class="layui-input" placeholder="按照月查" name="check_month" id="check_month" >
          <input type="text" id="reason" name="reason"  placeholder="按缺勤原因" autocomplete="off" class="layui-input" style="width: 400px;">
          <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
        </form>
      </div>
      
      <!-- 补考勤 -->
      <xblock>
        <button class="layui-btn layui-btn-danger" onclick="add()"><i class="layui-icon">&#xe654;</i>补考勤</button>
        <!-- <button class="layui-btn" onclick="x_admin_show('添加用户','./order-add.html')"><i class="layui-icon"></i>添加</button> -->
        <span class="x-right" style="line-height:40px">共有数据：${pageInfo.total } 条</span>
      </xblock>
      
      
      <!-- 列表 -->
      <table class="layui-table">
        <thead>
          <tr>
            <th>日期</th>
            <th>打卡时间</th>
            <th>打卡类型</th>
            <th>异常原因</th>
            <th>领导意见</th>
            <th>审批时间</th>
            <th>领导名称</th>
           <!--  <th >操作</th> -->
         </tr>
        </thead>
        <tbody>
        <c:if test="${pageInfo!=null }">
	        <c:forEach items="${pageInfo.list }" var="a">
	          <tr>
	            <td>${a.attendanceDay }</td>
	            <td>${a.attendanceEvery }</td>
	            <td>
	             <c:if test="${a.attendanceType==1 }">正常打卡</c:if>
	             <c:if test="${a.attendanceType==2 }">无故缺勤</c:if>
	             <c:if test="${a.attendanceType==3 }">未被确认</c:if>
	             <c:if test="${a.attendanceType==4 }">因故缺勤</c:if>
	            </td>
	            <td>${a.exceptionExplain }</td>
	            <td>${a.approvalOpinion }</td>
	            <td><fmt:formatDate value="${a.opinionTime }" pattern="yyyy-MM-dd"  /></td>
	            <td>${a.boss.rname }</td>
	            <!-- <td class="td-manage">
	              <a title="查看"  onclick="x_admin_show('编辑','order-view.html')" href="javascript:;">
	                <i class="layui-icon">&#xe63c;</i>
	              </a>
	              <a title="删除" onclick="member_del(this,'要删除的id')" href="javascript:;">
	                <i class="layui-icon">&#xe640;</i>
	              </a>
	            </td> -->
	          </tr>
	          </c:forEach>
          </c:if>
        </tbody>
      </table>
      
      <c:if test="${pageInfo!=null }">
	      <div class="page">
	        <div>
	          <a class="prev" href="${pageContext.request.contextPath }/findAttendance?check_date=${date }&check_month=${month }&reason=${reason }&pageNum=${pageInfo.prePage }">&lt;&lt;</a>
	          <c:forEach begin="1" end="${pageInfo.pages }" step="1" var="i">
			  	<c:if test="${pageInfo.pageNum==i }">
			  		<span class="current">${i}</span>
			  	</c:if>
			  	<c:if test="${pageInfo.pageNum!=i }">
			  		<a class="num" href="${pageContext.request.contextPath }/findAttendance?check_date=${date }&check_month=${month }&reason=${reason }&pageNum=${i}">${i}</a>
			  	</c:if>
			  </c:forEach>
	          <a class="next" href="${pageContext.request.contextPath }/findAttendance?check_date=${date }&check_month=${month }&reason=${reason }&pageNum=${pageInfo.nextPage }">&gt;&gt;</a>
	        </div>
	      </div>
      </c:if>
      
      
      <!-- 列表  2-->
      <table class="layui-table">
        <thead>
          <tr>
            <th colspan="4">该月总打卡数：<span style="color: blue;">${monthCount.totalCount }</span>(次)</th>
		  </tr>
          <tr>
            <th colspan="1">准点</th>
            <th colspan="3">迟到：<span style="color: blue;">${monthCount.outTimeWithoutReason + monthCount.outTimeWithReasonAndWithoutAgree + monthCount.outTimeWithReasonAndWithAgree }</span>(次)</th>
         </tr>
        </thead>
        <tbody>
	          <tr>
	            <td rowspan="2"><span style="color: blue;">${monthCount.onTimeCount }</span>(次)</td>
	            <td>无故</td>
	            <td>因故</td>
	            <td>未被确认</td>
	          </tr>
	          <tr>
	            <td><span style="color: blue;">${monthCount.outTimeWithoutReason }</span>(次)</td>
	            <td><span style="color: blue;">${monthCount.outTimeWithReasonAndWithAgree }</span>(次)</td>
	            <td><span style="color: blue;">${monthCount.outTimeWithReasonAndWithoutAgree }</span>(次)</td>
	          </tr>
        </tbody>
      </table>
      
      
      
      

    </div>
    <script>
      layui.use('laydate', function(){
        var laydate = layui.laydate;
        
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });

       /*用户-停用*/
      function member_stop(obj,id){
          layer.confirm('确认要停用吗？',function(index){

              if($(obj).attr('title')=='启用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 5,time:1000});

              }else{
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 5,time:1000});
              }
              
          });
      }

      /*用户-删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              $(obj).parents("tr").remove();
              layer.msg('已删除!',{icon:1,time:1000});
          });
      }



      function add() {
    	  layer.open({
    	      type: 2,
    	      title: '补考勤',
    	      maxmin: true,
    	      shadeClose: true, //点击遮罩关闭层
    	      area : ['800px' , '420px'],
    	      content: ['${pageContext.request.contextPath }/redirect/toAddAttendance','no']
    	  });
      }
      
      //提交
      function Submit(){
    	var dateValue = $("#check_date").val();
    	var monthValue = $("#check_month").val();
    	if(dateValue.length!=0 && monthValue.length!=0){
    		layer.msg("不能同时指定日期和指定月份");
    		return false;
    	}
    	return true;
      }
    </script>

  </body>

</html>