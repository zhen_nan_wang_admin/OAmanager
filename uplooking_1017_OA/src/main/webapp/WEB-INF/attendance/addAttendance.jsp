<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta charset="utf-8">
	<title>OA管理后台</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/font.css">
	<link rel="stylesheet" href="css/xadmin.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/layer/layer.js"></script>
    <script src="lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>
    <script type="text/javascript">
    layui.use('laydate', function(){
    	  var laydate = layui.laydate;
    	  laydate.render({
    	    elem: '#date' //指定元素
    	  });
    });
    </script>


</head>
<body >
	<div>
	<!-- onsubmit="return SubmitCheck()" -->
	<form class="layui-form" action="checkUser" id="form" onsubmit="return SubmitAdd()">
		<div style="margin-left: 150px;margin-top: 100px; ">
		  <div class="layui-form-item">
		    <label class="layui-form-label">缺勤日期</label>
		    <div class="layui-input-block">
		      <input id="date" type="text" name="date" required  lay-verify="required" placeholder="请输入缺勤日期" autocomplete="off" class="layui-input" style="width: 300px;">
		    </div>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">缺勤原因</label>
		    <div class="layui-input-inline">
		      <input id="reason" type="text" name="reason" required lay-verify="required" placeholder="请输入缺勤原因" autocomplete="off" class="layui-input" style="width: 300px;">
		    </div>
		  </div>
		  <div class="layui-form-item" id="submitDiv" style="margin-left: 65px;">
		    <div class="layui-input-block">
		      <button class="layui-btn" lay-submit lay-filter="formDemo">提交</button>
		      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		  </div>
		</div>
	</form>
	</div>
</body>
<script type="text/javascript">
function SubmitAdd(){
	$.ajax({
		url:"reviewAttendance",
		type:"post",
		data:$("#form").serialize(),
		success:function(result){
			var obj=eval("("+result+")");
			if(obj.head=="ok"){
				layer.msg(obj.msg,{icon:16,time: 1*1000},function(){
					var index = layer.index;
					window.parent.location.reload();
					parent.layer.close(index);
					
				});
			}else {
				layer.msg(obj.msg);
			}
		}
	});
	//ajax添加 ，不需要跳转页面 
	return false;
	
}

</script>


</html>