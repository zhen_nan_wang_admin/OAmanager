<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!doctype html>
<html lang="en">
<head>
	<base href="<%=basePath%>">
	<meta charset="UTF-8">
	<title>OA管理后台</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/font.css">
	<link rel="stylesheet" href="css/xadmin.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/layer/layer.js"></script>
    <script src="lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>
    
</head>
<body class="login-bg">
    
    <div class="login layui-anim layui-anim-up">
        <div class="message">OA考勤管理后台</div>
        <div id="darkbannerwrap"></div>
        ${loginMsg }
        <form method="post" id="form" class="layui-form" action="${pageContext.request.contextPath }/login" onsubmit="return SubmitLogin()">
            <input name="uname" placeholder="用户名"  type="text" lay-verify="required" class="layui-input" >
            <hr class="hr15">
            <input name="pwd" lay-verify="required" placeholder="密码"  type="password" class="layui-input">
            <hr class="hr15">
            <input value="登录" lay-submit lay-filter="login" style="width:100%;" type="submit">
            <hr class="hr">
            <span style="margin-left: 270px;"><a id="forgetPwd" href="#">忘记密码？</a></span>
        </form>
    </div>

    <script>
        $(function(){
        	$('#forgetPwd').on('click', function(){
        	    layer.open({
        	      type: 2,
        	      title: '忘记密码',
        	      maxmin: true,
        	      shadeClose: true, //点击遮罩关闭层
        	      area : ['800px' , '520px'],
        	      content: ['${pageContext.request.contextPath }/redirect/toForgetPwd','no']
        	    });
        	 });
        }) 
         
        function SubmitLogin(){
        	$.ajax({
        		url:"login",
        		type:"post",
        		data:$("#form").serialize(),
        		success:function(result){
        			var obj=eval("("+result+")");
        			if(obj.head=="ok"){
        				layer.msg(obj.body,{icon:16,time: 1*1000},function(){
        					window.location.href="${pageContext.request.contextPath }/redirect/toIndex";
	    				});
        			}else {
        				layer.msg(obj.body);
        			}
        		}
        	});
        	
        	//ajax添加 ，不需要跳转页面 
        	return false;
        }
        
    </script>

    
    <!-- 底部结束 -->

</body>
</html>