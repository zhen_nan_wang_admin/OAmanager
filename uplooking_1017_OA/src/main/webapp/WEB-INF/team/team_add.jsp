<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
  <head>
  	<base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/layer/layer.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <div class="x-body">
        <form class="layui-form" id="form" action="add_user" onsubmit="return AddUser()">
          <!-- 登陆名 -->
          <div class="layui-form-item">
              <label for="uname" class="layui-form-label">
                  <span class="x-red">*</span>登录名
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="uname" name="uname" required="" lay-verify="required"
                  autocomplete="off" class="layui-input">
              </div>
              <div class="layui-form-mid layui-word-aux">
                  <span class="x-red">*</span>将会成为您唯一的登入名
              </div>
          </div>
          <!-- 真实姓名 -->
          <div class="layui-form-item">
              <label for="rname" class="layui-form-label">
                  <span class="x-red">*</span>真实姓名
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="rname" name="rname" required="" lay-verify="required"
                  autocomplete="off" class="layui-input">
              </div>
          </div>
          <!-- 密码 -->
          <div class="layui-form-item">
              <label for="pwd" class="layui-form-label">
                  <span class="x-red">*</span>密码
              </label>
              <div class="layui-input-inline">
                  <input type="password" id="pwd" name="pwd" required="" lay-verify="required"
                  autocomplete="off" class="layui-input">
              </div>
          </div>
          <!-- 手机 -->
          <div class="layui-form-item">
              <label for="mobile" class="layui-form-label">
                  <span class="x-red">*</span>手机
              </label>
              <div class="layui-input-inline">
                  <input type="text" id=mobile name="mobile" required="" lay-verify="mobile"
                  autocomplete="off" class="layui-input">
              </div>
          </div>
          <!-- 邮箱 -->
          <div class="layui-form-item">
              <label for="email" class="layui-form-label">
                  <span class="x-red">*</span>邮箱
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="email" name="email" required="" lay-verify="email"
                  autocomplete="off" class="layui-input">
              </div>
              <div class="layui-form-mid layui-word-aux">
                  <span class="x-red">*</span>
              </div>
          </div>
          <!-- 备注 -->
          <div class="layui-form-item">
              <label for="usrFlag" class="layui-form-label">
                  <span class="x-red">*</span>备注
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="usrFlag" name="usrFlag" required="" lay-verify="usrFlag"
                  autocomplete="off" class="layui-input">
              </div>
              <div class="layui-form-mid layui-word-aux">
                  <span class="x-red">*</span>
              </div>
          </div>
          <!-- 部门 -->
          <div class="layui-form-item">
              <label class="layui-form-label"><span class="x-red">*</span>部门</label>
              <div class="layui-input-block">
              <c:if test="${user.aid==3 && user.gid==1 }">
                <input type="radio" name="gid" value="1" lay-skin="primary" title="人事部" checked="checked">
                <input type="radio" name="gid" value="2" lay-skin="primary" title="开发部" disabled="disabled">
              </c:if>
              <c:if test="${user.aid==3 && user.gid==2 }">
                <input type="radio" name="gid" value="1" lay-skin="primary" title="人事部" disabled="disabled">
                <input type="radio" name="gid" value="2" lay-skin="primary" title="开发部" checked="checked">
              </c:if>
              <c:if test="${user.aid==1 }">
                <input type="radio" name="gid" value="1" lay-skin="primary" title="人事部" checked="checked">
                <input type="radio" name="gid" value="2" lay-skin="primary" title="开发部" >
              </c:if>
              </div>
          </div>
          <!-- 权限 -->
          <div class="layui-form-item">
              <label class="layui-form-label"><span class="x-red">*</span>部门</label>
              <div class="layui-input-block">
              <c:if test="${user.aid==3 }">
                <input type="radio" name="aid" value="2" lay-skin="primary" title="员工" checked="checked">
                <input type="radio" name="aid" value="3" lay-skin="primary" title="经理" disabled="disabled">
                <input type="radio" name="aid" value="1" lay-skin="primary" title="老板" disabled="disabled">
              </c:if>
              <c:if test="${user.aid==1 }">
                <input type="radio" name="aid" value="1" lay-skin="primary" title="员工" checked="checked">
                <input type="radio" name="aid" value="2" lay-skin="primary" title="经理" >
                <input type="radio" name="aid" value="2" lay-skin="primary" title="老板" >
              </c:if>
              </div>
          </div>
          
          <div class="layui-form-item">
              <label for="L_repass" class="layui-form-label">
              </label>
              <button  class="layui-btn" lay-filter="add" lay-submit="">增加</button>
          </div>
          
          
      </form>
    </div>
    <script>
        layui.use(['form','layer'], function(){
          $ = layui.jquery;
          var form = layui.form
          ,layer = layui.layer;
        
          //自定义验证规则
          form.verify({
            nikename: function(value){
              if(value.length < 5){
                return '昵称至少得5个字符啊';
              }
            }
            ,pass: [/(.+){6,12}$/, '密码必须6到12位']
            ,repass: function(value){
                if($('#L_pass').val()!=$('#L_repass').val()){
                    return '两次密码不一致';
                }
            }
            ,mobile: [/^1(3|4|5|7|8)\d{9}$/, '手机号码格式不正确']
          });

        });
        
     function AddUser(){
    	 $.ajax({
    			url:"add_user",
    			type:"post",
    			data:$("#form").serialize(),
    			success:function(result){
    				var obj=eval("("+result+")");
    				if(obj.head=="ok"){
    					layer.msg(obj.msg,{icon:16,time: 1*1000},function(){
    						window.location.href="${pageContext.request.contextPath }/redirect/toTeamList";
    					});
    				}else {
    					layer.msg(obj.msg);
    				} 
    			}
    		});
    		//ajax添加 ，不需要跳转页面 
    	return false; 
     }   
        
    </script>

  </body>

</html>