<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
  
<head>
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/layer/layer.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <script type="text/javascript">
    </script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <c:if test="${userPageInfo==null }">
         <% request.getRequestDispatcher("/boss_team_list").forward(request, response); %>
    </c:if>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a><cite>员工列表</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    <div class="x-body">
    
    
    
      <!-- 按照队员名称模糊查询 -->
      <div class="layui-row">
      
        <form class="layui-form layui-col-md12 x-so layui-form-pane" action="${pageContext.request.contextPath }/boss_team_list" style="margin-left: 100px;">
		      <div class="" style="width: 30%;margin-left: -100px;float: left;">
			    <label class="layui-form-label" >部门</label>
			    <div class="layui-input-block" style="width: 150px;" >
			    <select name="gid"  id="group" lay-filter="group">
			        <option value="1" ${gid==1?'selected="selected"':'' }>人事部</option>
			        <option value="2" ${gid==2?'selected="selected"':'' }>开发部</option>
			    </select>
		        </div>
			  </div >
	          <div class="" style="width: 25%;float: left;" >
			    <label class="layui-form-label">按名称搜索</label>
			    <div class="layui-input-block">
	         	 <input class="layui-input" placeholder="用户名或者真实姓名" name="name" style="width: 205px;">
	          	</div>
			  </div>
	          <button class="layui-btn" type="submit" lay-filter="sreach"><i class="layui-icon">&#xe66f;</i>查询</button>
        </form>
      </div>
      
      
      <!-- 批量删除 -->
      <%-- <xblock>
        <button class="layui-btn layui-btn-danger" onclick="delAll()"><i class="layui-icon"></i>批量删除</button>
        <span class="x-right" style="line-height:40px">共有数据：${userPageInfo.total } 条</span>
      </xblock> --%>
      
      <!-- 队员列表  -->
      <table class="layui-table">
        <thead>
          <tr>
            <!-- <th>
              <div class="layui-unselect header layui-form-checkbox" lay-skin="primary"><i class="layui-icon">&#xe605;</i></div>
            </th> -->
            <th>ID</th>
            <th>用户名称</th>
            <th>真实姓名</th>
            <th>岗位</th>
            <th>手机号码</th>
            <th>邮箱</th>
            <th>入职时间</th>
            <th>部门</th>
            <th>备注</th>
            <th>查看考勤</th>
            <th>操作</th>
		  </tr>            
        </thead>
        <tbody>
	        <c:if test="${userPageInfo!=null }">
	        <c:forEach items="${userPageInfo.list }" var="u">
	          <tr>
	           <!--  <td>
	              <div class="layui-unselect layui-form-checkbox" lay-skin="primary" data-id='2'><i class="layui-icon">&#xe605;</i></div>
	            </td> -->
	            <td>${u.uid }</td>
	            <td>${u.uname }</td>
	            <td>${u.rname }</td>
	            <td><c:if test="${u.aid==2 }">员工</c:if><c:if test="${u.aid==3 }">经理</c:if></td>
	            <td>${u.mobile }</td>
	            <td>${u.email }</td>
	            <td><fmt:formatDate value="${u.createTime }" pattern="yyyy-MM-dd"/></td>
	            <td><c:if test="${u.gid==1 }">人事部</c:if><c:if test="${u.gid==2 }">开发部</c:if></td>
	            <td>${u.usrFlag }</td>
	            <td style="text-align: center;"><input id="${u.uid }" class="layui-btn layui-btn-xs layui-icon findAttendance" onclick="" value="&#xe64c;查询" style="width: 70px;"></td>
	            <td class="td-manage">
	              <a id="${u.uid }" class="updateUser" title="编辑"  onclick="UpdateUser()" href="javascript:;"><i class="layui-icon">&#xe642;</i></a>
	              <!-- <a title="删除" onclick="member_del(this,'要删除的id')" href="javascript:;">
	                <i class="layui-icon">&#xe640;</i>
	              </a> -->
	            </td>
	          </tr>
	        </c:forEach>
	        </c:if>
        </tbody>
      </table>
      
      
      <c:if test="${userPageInfo!=null }">
	      <div class="page">
	        <div>
	          <a class="prev" href="${pageContext.request.contextPath }/boss_team_list?pageNum=${userPageInfo.prePage }&name=${name }&gid=${gid }">&lt;&lt;</a>
	          <c:forEach begin="1" end="${userPageInfo.pages }" step="1" var="i">
			  	<c:if test="${userPageInfo.pageNum==i }">
			  		<span class="current">${i}</span>
			  	</c:if>
			  	<c:if test="${userPageInfo.pageNum!=i }">
			  		<a class="num" href="${pageContext.request.contextPath }/boss_team_list?pageNum=${i}&name=${name }&gid=${gid }">${i}</a>
			  	</c:if>
			  </c:forEach>
	          <a class="next" href="${pageContext.request.contextPath }/boss_team_list?pageNum=${userPageInfo.nextPage }&name=${name }&gid=${gid }">&gt;&gt;</a>
	        </div>
	      </div>
      </c:if>

    </div>
    <script>
      layui.use('laydate', function(){
        var laydate = layui.laydate;
        
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });
      
      /*select选中事件*/
      layui.use(['form'], function() {
         form=layui.form;
		 form.on('select(group)', function(data){   
			  var val=data.value;
			  window.location.href="${pageContext.request.contextPath }/boss_team_list?gid="+val;
			   
		 });
      })

      

       /*用户-停用*/
      function member_stop(obj,id){
          layer.confirm('确认要停用吗？',function(index){

              if($(obj).attr('title')=='启用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 5,time:1000});

              }else{
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 5,time:1000});
              }
              
          });
      }

      /*用户-删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              $(obj).parents("tr").remove();
              layer.msg('已删除!',{icon:1,time:1000});
          });
      }
      /*用户-删除所有*/
      function delAll (argument) {

        var data = tableCheck.getData();
  
        layer.confirm('确认要删除吗？'+data,function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
      }
      
      /*用户-查看考勤*/
      $(function(){
    	  $(".findAttendance").click(function(){
				//通过按钮获取用户id
				var userId = this.id;
				layer.open({
		    	      type: 2,
		    	      title: '查看个人考勤',
		    	      maxmin: true,
		    	      shadeClose: true, //点击遮罩关闭层
		    	      area : ['800px' , '520px'],
		    	      content: ['${pageContext.request.contextPath }/check_user_attendance?uid='+userId,'no']
		    	  });
				
    	  });
    	  
    	  $(".updateUser").click(function(){
				//通过按钮获取用户id
				var userId = this.id;
				//layer.msg(userId);
				 layer.open({
		    	      type: 2,
		    	      title: '编辑用户',
		    	      maxmin: true,
		    	      shadeClose: true, //点击遮罩关闭层
		    	      area : ['800px' , '600px'],
		    	      content: ['${pageContext.request.contextPath }/redirect/to_update_user?uid='+userId,'no']
		    	  });
  	      });
      });
      
      function ChangGroup(){
    	  layer.msg("嘿嘿")
      }
      
      
      
    </script>

  </body>

</html>